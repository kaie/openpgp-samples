#!/usr/bin/make -f
#
# Prerequisites: apt install ruby-kramdown-rfc2629 xml2rfc
#

draft = draft-openpgp-samples
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf
dependencies = $(foreach person,alice bob,$(foreach suffix,pub sec rev,$(person)@openpgp.example.$(suffix).asc))

all: $(OUTPUT)

$(draft).md: $(draft).in.md assemble $(dependencies)
	echo $(dependencies)
	./assemble < $< >$@.tmp
	mv $@.tmp $@

%.xmlv2: %.md
	kramdown-rfc2629 < $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	xml2rfc --v2v3 -o $@ $<

%.html: %.xml
	xml2rfc $< --html

%.pdf: %.xml
	xml2rfc $< --pdf

%.txt: %.xml
	xml2rfc $< --text

clean:
	-rm -rf $(OUTPUT) $(draft).xmlv2 $(draft).md

.PHONY: clean all
.SECONDARY: $(draft).xmlv2
